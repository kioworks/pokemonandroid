package com.example.pokdex.database;

import android.app.Application;
import android.os.AsyncTask;


import com.example.pokdex.PokemonRoomDatabase;

import java.util.concurrent.ExecutionException;


public class PokemonRepository {

    private PokemonDao pokemonDao;


    public PokemonRepository(Application application) {
        PokemonRoomDatabase db = PokemonRoomDatabase.getDatabase(application);
        pokemonDao = db.pokemonDao();
    }
    public void insert(PokemonEntities pokemonEntities) {
        new PokemonRepository.insertAsyncTask(pokemonDao).execute(pokemonEntities);
    }

    //Denna är fel på

    //public PokemonEnities selectPokemon(String name) throws ExecutionException, InterruptedException {
    // return new PokemonRepository.selectPokemonAsyncTask(pokemonDao.execute(name).get());
    //}

    public PokemonEntities[] getAllPokemon() throws ExecutionException, InterruptedException {

        return new PokemonRepository.getAllpokemonAsyncTask(pokemonDao).execute().get();
    }

    public void deleteAllPokemon() {
        new PokemonRepository.deleteAllPokemonAsyncTask(pokemonDao).execute();
    }


    private class insertAsyncTask extends AsyncTask<PokemonEntities, Void, Void> {
        private PokemonDao pokemonDao;

        public insertAsyncTask(PokemonDao pokemonDao) {
            this.pokemonDao = pokemonDao;
        }

        @Override
        protected Void doInBackground(PokemonEntities... starWarsShips) {
            pokemonDao.insert(starWarsShips[0]);
            return null;
        }
    }

    private class deleteAllPokemonAsyncTask extends AsyncTask<Void, Void, Void> {
        private PokemonDao pokemonDao;

        public deleteAllPokemonAsyncTask(PokemonDao pokemonDao) {
            this.pokemonDao= pokemonDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            pokemonDao.deleteAllPokemon();
            return null;
        }

    }

    private class selectPokemonAsyncTask extends AsyncTask<String, Void, PokemonEntities> {
        private PokemonDao pokemonDao;

        public selectPokemonAsyncTask(PokemonDao pokemonDao) {
            this.pokemonDao = pokemonDao;
        }

        @Override
        protected PokemonEntities doInBackground(String... strings) {
            return pokemonDao.getPokemonById(strings[0]);
        }
    }

    private class getAllpokemonAsyncTask extends AsyncTask<Void, Void, PokemonEntities[]> {
        private PokemonDao pokemonDao;

        public getAllpokemonAsyncTask(PokemonDao pokemonDao) {
            this.pokemonDao = pokemonDao;
        }

        @Override
        protected PokemonEntities[] doInBackground(Void... voids) {
            return pokemonDao.getAllPokemon();
        }


    }
}