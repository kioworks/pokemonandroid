package com.example.pokdex.firstpage;

import com.example.pokdex.models.PokemonObjects;
import com.example.pokdex.models.ResultsContainer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonHolder {
    @GET("pokemon-species/")
    Call<ResultsContainer> getAllPokemon(@Query("limit") Integer limit);

    @GET("pokemon-species/{id}")
    Call <PokemonObjects> getPokemonSpeciesInfo(@Path("id") Integer id);

    @GET("pokemon/{id}")
    Call <PokemonObjects> getOtherPokemonInfo(@Path("id") Integer id);



}

