package com.example.pokdex.firstpage;


import android.util.Log;

import com.example.pokdex.models.ResultModel;
import com.example.pokdex.models.ResultsContainer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonListLoader {

    private final String TAG = "List";
    private static final Integer numberOfPokemon = 900;

    /*
     * This method will be used for injection, but later on will be used to save into DB
     */
    public void getAllPokemon(final TaskComplete taskComplete) {
        Call<ResultsContainer> call = getJson().getAllPokemon(numberOfPokemon);
        call.enqueue(new Callback<ResultsContainer>() {
            @Override
            public void onResponse(Call<ResultsContainer> call, Response<ResultsContainer> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Error: " + response.errorBody());
                }
                assert response.body() != null;
                List<ResultModel> results = response.body().getResults();
                taskComplete.onSuccess(results);

            }
            @Override
            public void onFailure(Call<ResultsContainer> call, Throwable t) {
               t.printStackTrace();
               call.cancel();

            }
        });


    }


    //Helper methods

    private JsonHolder getJson() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(JsonHolder.class);
    }

}
