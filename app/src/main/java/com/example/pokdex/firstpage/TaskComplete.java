package com.example.pokdex.firstpage;

import com.example.pokdex.models.ResultModel;

import java.util.List;

public interface TaskComplete {
    void onSuccess(List<ResultModel> result);
}
