package com.example.pokdex.models;

import com.google.gson.JsonObject;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class GeneralObject {

    private final String flavor_text;
    private final JsonObject language;
    private final String genus;
    private final String description;
    private final String front_default;
    private final ResultModel type;
    private final String name;
    private final String Url;


}
