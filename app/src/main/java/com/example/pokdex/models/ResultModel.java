package com.example.pokdex.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ResultModel {
    private final String name;
    private final String url;
}
